import React, { useState, useEffect } from "react";
import axios from "axios";
import { View, Text, ScrollView, StyleSheet } from "react-native";
import { Loading } from "../components/Loading";
import { Card, Button, Icon } from "react-native-elements";

export const Products = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const API = "https://jsonplaceholder.typicode.com/users";

  useEffect(() => {
    (async () => {
      const res = await axios.get(API)
      console.log(res.data)
      setLoading(false);
    })();
  }, []);

  if (loading) return <Loading text="Cargando" isVisible={true} />;

  return (

    <ScrollView>
      <View>
        {data.map((movie, i) => (
          <Card title={movie.Title} image={{ uri: movie.Poster }} key={i}>
            <Text style={{ marginBottom: 10 }}>
              {movie.Type} {movie.Year}
            </Text>
            <Button buttonStyle={styles.btnAdd} title="Añadir al carrito" />
          </Card>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    borderRadius: 7,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    backgroundColor: "#57C47A",
  },
});
