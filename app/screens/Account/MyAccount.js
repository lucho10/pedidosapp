import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";

export const MyAccount = () => {
  const [login, setlogin] = useState(null);

  useEffect(() => {
    setlogin(true);
  }, []);

  if (login === null) {
    return (
      <View>
        <Text>Cargando</Text>
      </View>
    );
  }

  if (login) {
    return (
      <View>
        <Text>My Account</Text>
      </View>
    );
  }
};
