import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { MaterialCommunityIcons } from "react-native-vector-icons";
import { Icon } from "react-native-elements";

import { Products } from "../screens/Products";
import { Cart } from "../screens/Cart";
import { MyAccount } from "../screens/Account/MyAccount";
import { Login } from "../screens/Login";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const ProductsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Productos" component={Products} />
    </Stack.Navigator>
  );
};

const CartStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Carrito" component={Cart} />
    </Stack.Navigator>
  );
};
const MyAccountStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="My Account" component={MyAccount} />
    </Stack.Navigator>
  );
};

const LoginStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

export const Navigation = () => {
  const user = true;

  return (
    <NavigationContainer>
      {user ? (
        <Tab.Navigator
          initialRouteName="Productos"
          tabBarOptions={{
            activeTintColor: "#00a680",
            inactiveTintColor: "#646464",
          }}
        >
          <Tab.Screen
            name="Productos"
            component={ProductsStack}
            options={{
              tabBarIcon: ({ color, size }) => (
                <Icon
                  type="material-community"
                  name="home"
                  size={size}
                  color={color}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Carrito"
            component={CartStack}
            options={{
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="cart" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen
            name="My Account"
            component={MyAccountStack}
            options={{
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons
                  name="account"
                  color={color}
                  size={size}
                />
              ),
            }}
          />
        </Tab.Navigator>
      ) : (
        <LoginStack />
      )}
    </NavigationContainer>
  );
};
