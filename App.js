import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, Icon } from "react-native-elements";
import { Navigation } from "./app/navigation/Navigation";

export default function App() {
  return (
    <Navigation />
  );
}

